from views import index, handler, fstateHandler, startHandler, restartHandler, stopHandler


def setup_routes(app):
    app.router.add_get('/', handler)
    app.router.add_post('/fState', fstateHandler)
    app.router.add_post('/start', startHandler)
    app.router.add_post('/restart', restartHandler)
    app.router.add_post('/stop', stopHandler)
