from aiohttp import web
import aiohttp_jinja2
import jinja2
import subprocess
import re
import time

async def index(request):
    return web.Response(text='Hello Aiohttp!')

@aiohttp_jinja2.template('index.html')
def handler(request):
    return reloadTemplate(request)

async def fstateHandler(request):
    data = await request.post()
    print (data['flag'])
    f = open('flag.txt', 'w')
    if re.match('true', data['flag']) != None:
        f.write('1')
    else:
        f.write('0')
    f.close()
    return web.Response(status=200)


@aiohttp_jinja2.template('index.html')
async def startHandler(request):
    subprocess.run(["sudo", "nginx"])
    return reloadTemplate(request)

async def restartHandler(request):
    subprocess.run(["sudo", "nginx", "-s", "quit"])
    subprocess.run(["sudo", "nginx"])
    return reloadTemplate(request)
    
async def stopHandler(request):
    subprocess.run(["sudo", "nginx", "-s", "quit"])
    return reloadTemplate(request)

#Process list parser

def reloadTemplate(request):
    processList = ParseProcessList(GetProcessList())#Имеем список строк
    f = open('flag.txt', 'r')
    fState = f.read()
    f.close()
    if re.match('1', fState) != None:
        prKeys = True
    else:
        prKeys = False
    return {'processList':processList, 'fstate':prKeys}



def GetProcessList():
    """Цель данной функции - получить и декодировать список процессов. Нужно для отслеживания состояния демона. Если, после переразбиения, в нем будет содержаться """
    pLine = subprocess.check_output(["ps", "-axo", "command"])
    return pLine.decode("utf-8")

def ParseProcessList(pList):
    """Разбивает строку по переводам строки, потом делит каждую подстроку на части и формирует словарь"""
    toReturn = False
    proc = pList.split("\n")
    proc.pop(0)
    for i in proc:
        result = re.sub(r'^\s+', ' ', i)
        procLine = re.split(r'\s+', result)
        try:
            if re.match('nginx:', procLine[0]) != None:
                toReturn = True
                break
            else:
                toReturn = False
        except IndexError:
            break
    return toReturn 

