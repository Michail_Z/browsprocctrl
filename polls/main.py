import asyncio
import aiohttp_jinja2
import jinja2
from aiohttp import web
from routes import setup_routes
import aiohttp_debugtoolbar
from aiohttp_debugtoolbar import toolbar_middleware_factory


loop = asyncio.get_event_loop()
app = web.Application(loop=loop, middlewares=[toolbar_middleware_factory])
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
aiohttp_debugtoolbar.setup(app)
setup_routes(app)
web.run_app(app, host='127.0.0.1', port=8080)

